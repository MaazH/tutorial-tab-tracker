
import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Register from '@/components/Register'
import Login from '@/components/Login'
import Songs from '@/components/Songs'
import CreateSong from '@/components/CreateSong'
import AdminPanel from '@/components/AdminPanel'

Vue.use(Router)
var hasUserLogged = false;
import store from '../store/store.js'


const routes =  [
  {
    path: '/',
    name: 'root',
    component: HelloWorld,
  },
  {
    path: '/register',
    name:'register',
    component: Register
  },
  {
    path: '/login',
    name:'login',
    component: Login
  },
  {
    path: '/songs',
    name:'songs',
    component: Songs,
    meta: { AdminAuth: true}
  },
  {
    path: '/createSong',
    name:'songs-create',
    component: CreateSong,
     meta: { EditorAuth: true}
  
  },
  {
    path: '/admin',
    name:'admin-panel',
    component: AdminPanel,
    meta: { requiresAuth: true}
  }
]
// inital mode was history
const router = new Router({routes,mode:'history'})  

router.beforeEach((to, from, next) => {
  //const authUser = JSON.parse(window.localStorage.getItem('lbUser'))
  if(to.meta.requiresAuth) {
		if(store.state.isUserLoggedIn) {
		  next()
		}else{
			next({name:'login'})
		} 
  }
	else if(to.meta.AdminAuth){
		if(store.state.isUserLoggedIn && store.state.user.userrole == 'admin') {
		  next()
		}else{
			console.log('You aint an admin')
			next({name:'login'})
		} 
  
	}else if(to.meta.EditorAuth){
		if(store.state.isUserLoggedIn && ((store.state.user.userrole == 'editor') || (store.state.user.userrole == 'admin')))  {
		  next()
		}
		else{
			console.log('You aint even an editor')
			next({name:'login'})
		} 
	}
  else{
	  next()
  }
})

export default router;