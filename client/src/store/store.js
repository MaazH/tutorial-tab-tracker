import Vue from 'vue'
import Vuex from 'vuex'
import router from '@/router/index'
// tell vue to use vuex - allow vue to access and store vue components
Vue.use(Vuex)


// Vuex works have action mutatuon state. Action is function. 
export default new Vuex.Store({
    // cannot modify state unless action or mutation
    strict: true,
    // these are global states that the app has. 
    state: {
        token:  localStorage.getItem('token') || '',
        user: null,
        // state to know 
        isUserLoggedIn: false,
        isUserAdmin: false,
        isUserEditor: false
    },
    mutations: { 
        setToken(state, token){
            state.token = token
            //checking
            localStorage.setItem('token', token);
            // if the token is not null then the user is logged in-conditiobn
            if(token){
                state.isUserLoggedIn = true
                router.hasUserLogged = true
            } else {
                state.isUserLoggedIn = false
            }
        },
        setUser(state, user){
            state.user = user
        }
    },
    actions: {
        // first perform an action to mutate the token sta te to the value
        setToken({commit}, token){
            commit('setToken', token)
            
        },
        setUser({commit}, user){
            commit('setUser', user)
        }
    }
})