import Api from '@/services/Api'


// get the songs from the backend end points
export default {
  index(){
        return Api().get('songs')
    },
    post(song){
        return Api().post('songs',song)
    }      
}

//AuthenticationService.register({
 //   email: 'testing@gmail.com',
 //   password: "1234"
//})