module.exports = {
    port: process.env.PORT || 8081,
    db: {
        database: process.env.DB_NAME || 'tabtrackerthree',
        user: process.env.DB_USER || 'root',
        password: process.env.DB_PASS || 'maaz',
        options:{
            dialect: process.env.DIALECT || 'mysql',
            host: process.env.HOST || 'localhost',

        }
    },
// signing the jwt token - you need to pass a secret string which is only known by server
// which is used to determin whether the token is valid or not 
    authentication: {
        jwtSecret: process.env.JWT_SECRET || 'secret'
    }
}