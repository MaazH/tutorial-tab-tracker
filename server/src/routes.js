// contollers are used for declaring your end points
// while routes are used to point to the controllers 

const AuthenticationController = require('./controllers/AuthenticationController')
const AuthenticationControllerPolicy = require('./policies/AuthenticationControllerPolicy')
const SongsController =  require('./controllers/SongsController')

// How this works. is that we are going ot hit this endpoint  
// invoke the auth policy file which function - express middeware
// next would go to the next line-
module.exports = (app) => {
    app.post('/register', AuthenticationControllerPolicy.register,
    AuthenticationController.register)
    
    app.post('/login',
    AuthenticationController.login)

    app.get('/songs',
    SongsController.index)

    app.post('/songs',
    SongsController.post)
}


