// Controller is where you have all your end points defined
// export whatever routes should be assossiated with authentication
// so register should be here



const {User} = require('../models')
const jwt = require('jsonwebtoken')
const config = require('../config/config')

// function that is going to sign a json user object to give a jwt token 
function jwtSignUser (user) {
    const ONE_WEEK = 60 * 60 * 24 * 7
    return jwt.sign(user, config.authentication.jwtSecret, {
        expiresIn: ONE_WEEK
    })
}


// create a user that was using the body that was passed in with the body as a request
// and taht would create us a new usser
// and if there was an error. the user already exists, then 
module.exports = {
    async register(req, res) {
        try {
            // updating --
            // create a JWT token for register so that the user gets to login in easily

            const user = await User.create(req.body)
            const userJson = user.toJSON()
            // in the previous code only user model was created without token
            // now this makes it easier as the token is already created for him to login
            res.send({
                user: userJson,
                token: jwtSignUser(userJson)
            })
        } catch(err) {
            // if email already exists ..
            res.status(400).send({
                error: 'This email account was already in use. '
            })
        }

    },
    async login (req, res) {
        try {

            // over here the f irst thing we need to do is to find that user
            // whose credentials have just been added in and find the corrosp email
            // so grab
            const {email, password} = req.body
            const user = await User.findOne({
                where: {
                    email: email
                }
            })

            if (!user) { // CHECK if the emauil is found
                return res.status(403).send({
                    error: 'The login information was incorrect'
                })
            }
            // find if the password is valid
            // await because its going to return a promise
            const isPasswordValid = await user.comparePassword(password)
            if(!isPasswordValid) {
                console.log(isPasswordValid, password)

                return res.status(403).send({
                    error: 'The login information was incorrect. Password'
                })
            }
            
            const userJson = user.toJSON()
            res.send({
                user: userJson,
                token: jwtSignUser(userJson)
            })
        } catch(err) {
            res.status(500).send({
                error: 'An error has occured trying to login '
            })
        }        
    }
}
