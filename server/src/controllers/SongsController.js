// Song Model needs to be defined
// if we want to keep track of all our models in the DB then we need to define our form model

const {Song} = require('../models')


module.exports = {
    // creating a find method to find songs limited to 10 and retyrn that back in payload
    async index (req, res) {
        try {
            // create the song
            // create is going to pass our seequize object
            const songs = await Song.findAll({
                limit: 10
            })
            // limit give us 10 songs
            res.send(songs)
        } catch(err) {
            res.status(500).send({
                error: 'An error has occured trying to fetch the songs '
            })
        }        
    },
    // creating a post method to create new songs
    async post (req, res) {
        try {
            // create the song
            // create is going to pass our seequize object
            const song = await Song.create(req.body)
            res.send(song)
            
        } catch(err) {
            res.status(500).send({
                error: 'An error has occured trying to create a song '
            })
        }        
    }
}
