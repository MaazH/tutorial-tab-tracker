// middleware to create constraints
const Joi = require('joi')


// validate the email and password match couple of constrainst
module.exports = {
    register(req, res, next){
        const schema = {
            email: Joi.string().email(),
            password: Joi.string().regex(
                new RegExp('^[a-zA-Z0-9]{8,32}$')
            ),
            userrole: 'user'

        }

        // Use the schema that you just created and then decide to validate it. 
        const {error, value} = Joi.validate(req.body, schema) 

        if(error){
            // whuch error failerd
            switch(error.details[0].context.key){
                case 'email':
                    res.status(400).send({
                        error: 'Provide valid email address'
                    })
                    break
                case 'password':
                    res.status(400).send({
                        error: `The password failed to match the following rules:
                        <br>
                        1.It must contain only the following cahractgers: Lower case, upper case, numerics
                        <br>
                        2. Must be 8 char in length and less than 32 character
                        `
                    })
                    break
                default:
                res.status(400).send({
                    error: 'Invalid registeration information'
                })
            }
        }else{
            next()
        }

    }
}