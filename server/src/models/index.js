const fs = require('fs') // filesystem
const path = require('path')
const Sequelize = require('sequelize')
const config = require('../config/config')
const db = {}

const sequelize = new Sequelize (
    config.db.database,
    config.db.user,
    config.db.password,
    config.db.options
)

// 12: 36
// read through the current directory and give us an array of different files. 
// we dont want index file but want every other model file
// for each file we found we want to go ahead and declare a model
// after file, for each file we found we will declare a model
// sequilize has a import method where you can specifiy the path which can be used ahead
// to link to the path
// set the db (name of model) to be equal to the model 
fs.readdirSync(__dirname)
.filter((file) =>
    file != 'index.js'
)
.forEach( (file) => { // for each file we found we need to devclare a model. Join dirname to file and tell sequlize to import to it
    const model = sequelize.import(path.join(__dirname, file))
    db[model.name] = model
})

db.sequelize = sequelize
db.Sequelize = sequelize

module.exports = db