// bcrypt for hashing and  bluebird for promises
// Promisify all which is going to taken in any function that has a callback structure
// and is going to wrap is and going to allow us to use the promise fornmat

const Promise = require('bluebird')
const bcrypt = Promise.promisifyAll(require('bcrypt-nodejs'))

// method for hashing password. SALT FACTOR idk what it does but is used bycrupt
function hashPassword(user, options){
    const SALT_FACTOR = 8

    // Check if password has been changed first

    // if pass hasnt changed just return
    if(!user.changed('password')){
        return;
    }
    // else take plain text password - hash it - and then update it
    return bcrypt.genSaltAsync(SALT_FACTOR)
    .then(salt => bcrypt.hashAsync(user.password, salt, null))
    .then(hash => {
        user.setDataValue('password', hash)
    })
}


module.exports = (sequelize, DataTypes ) => {
    
    // user object
const User = sequelize.define('User', {
        email: {
            type: DataTypes.STRING,
            unique: true
        },
        password: DataTypes.STRING,
        userrole: DataTypes.STRING
    },{
        // before we store the user, we need to hash the password
        hooks: {
            // call back funciot. hash pass before create, update, save
          //  beforeCreate: hashPassword,
          //  beforeUpdate: hashPassword,
          // these above hooks cause double hashing hence commented out. this is a bug.
            beforeSave: hashPassword
        }
    })

// attach a method on the prototype so that the user model is doing the password compare
// instead of the controller. its userful if we need to do password ocmpare anywhere else
User.prototype.comparePassword = function (password) {
   // compare the password to the models password
   // assuming the password is stored using the bycrypt now compare the pass
    return bcrypt.compareAsync(password, this.password)
}

// return user object
return User
}